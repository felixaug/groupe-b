# Titre du jeu

[[_TOC_]]

## Description

Jeu de thème mythologique, dans laquelle le but est de contrôler toutes les îles.

(_insérer un image_)

***

## Membres du projet

- Félix AUGAIS (Chef de projet)
- Bryan LEMAIRE (Développeur)
- Joshua TERRY (Développeur)

***

## Version

Le jeu est actuellement en version… (_à compléter_)

***

## Technologies

Le jeu utilise Python en version 3.# (_à compléter_) ou supérieur ainsi que les bibliothèques suivantes : 

- Tkinter
- (_à compléter_)

***

## Installation

_Décrire la procédure d'installation ici._
