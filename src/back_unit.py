from outils import *
from random import randint


class Unite:
    def __init__(self, unit_id: str):
        datas = charger_fichier_json("UNITS")[unit_id]
        self.etiquette = None
        self.x = 0
        self.y = 0
        self.id = unit_id
        self.name = datas["NOM"]
        self.nation = datas["NATION"]
        self.attack = datas["DEGATS"]
        self.max_hp = datas["VIE"]
        self.current_hp = datas["VIE"]
        self.mouvement_max = datas["MOVE"]
        self.mouvement = self.mouvement_max
        self.en_soin = False
        self.fortifie = False
        self.endormi = False
        self.attack_status = datas["CAN_ATTACK"]
        self.move_status = datas["CAN_MOVE"]
        self.range_status = datas["CAN_RANGE"]
        self.defend_status = datas["CAN_DEFEND"]
        # Images
        self.image = "../pics/units/" + datas["FICHIER"]

    # FONCTIONS D'AFFICHAGE
    def __str__(self) -> str:
        """Affiche le nom ainsi que les points d'attaque, de vie et de mouvement de self"""
        txt = "\t" + self.name + "\n"
        txt += "Attaque: " + str(self.attack) + "\n"
        txt += "Vie: " + str(self.current_hp) + "/" + str(self.max_hp) + "\n"
        txt += "Mouvements: " + str(self.mouvement)
        return txt

    def __repr__(self: list) -> str:
        """Affiche la liste des noms des unités"""
        return self.name

    # ACCESSEURS
    @property
    def est_mort(self) -> bool:
        """Vérifie si self n'a plus de pv"""
        if self.current_hp <= 0:
            return True
        else:
            return False

    @property
    def est_blesse(self) -> bool:
        """Vérifie si self est blessé (considéré comme blessé si ses pv ne sont pas au max)"""
        if self.current_hp == self.max_hp:
            return False
        else:
            return True

    @property
    def est_en_soin(self) -> bool:
        """Vérifie si self est en soin"""
        if self.en_soin == True:
            return True
        else:
            return False

    @property
    def est_fortifie(self) -> bool:
        """Vérifie si self est fortifié"""
        if self.fortifie == True:
            return True
        else:
            return False

    @property
    def est_endormi(self) -> bool:
        """Vérifie si self est endormi"""
        if self.endormi == True:
            return True
        else:
            return False

    @property
    def peut_agir(self) -> bool:
        """Vérifie si self peut jouer"""
        if self.est_mort or self.est_en_soin or self.est_fortifie or self.mouvement == 0 or self.est_endormi:
            return False
        else:
            return True

    def calcule_degats(self, other) -> int:
        """Calcule les dégâts que self infligerait à other s'il l'attaquait"""
        rapport = self.attack / other.attack
        hasard = 1 + randint(-10, 10) / 100
        if other.est_fortifie:
            degats_base = 40
        else:
            degats_base = 50
        degats = int(rapport * degats_base * hasard)
        return degats

    # MUTATEURS
    def obtient_etiquette(self, tag: str):
        """Attribue tag à self.etiquette"""
        self.etiquette = tag

    # Vie
    def enleve_pv(self, value: int):
        """Retire value à self.current_hp, sans atteindre un nombre négatif"""
        self.current_hp -= value
        if self.current_hp < 0:
            self.current_hp = 0

    def ajoute_pv(self, value: int):
        """Ajoute value à self.current_hp, sans dépasser max_hp"""
        self.current_hp += value
        if self.current_hp > self.max_hp:
            self.current_hp = self.max_hp

    def attaque_en_duel(self, other):
        """Les 2 unités s'infligent des dégâts, self attaque en premier"""
        self.attaque_a_distance(other)
        if not other.est_mort:
            other.attaque_a_distance(self)

    def attaque_a_distance(self, other):
        """self inflige des dégâts à other"""
        degats = self.calcule_degats(other)
        other.enleve_pv(degats)

    # Mouvements
    def perd_mouvement(self, value: int = 1):
        """Enlève un point de mouvement à self"""
        self.mouvement -= value
        if self.mouvement < 0:
            self.mouvement = 0

    def gagne_mouvement(self):
        """Ajoute un point de mouvement à self"""
        self.mouvement = self.mouvement_max

    def positionner(self, coordonnees: tuple):
        """Positionne self sur la carte"""
        self.x = coordonnees[0]
        self.y = coordonnees[1]

    # Statuts
    def se_reveille(self):
        """Enlève le statut endormi"""
        self.endormi = False

    def se_fortifie(self):
        """Fortifie self et le met en soin s'il est blessé"""
        self.fortifie = True
        self.endormi = False
        if self.est_blesse:
            self.en_soin = True

    def se_soigne(self):
        """Met self en soin et le dé-fortifie"""
        self.en_soin = True
        self.fortifie = False

    def s_endort(self):
        """Permet à self de passer son tour"""
        self.endormi = True
        self.en_soin = False
        self.fortifie = False
        self.mouvement = 0

    def commence_tour(self):
        """Commence un tour de jeu:
            -Réinitialise les mouvements
            -Réveille self
            -Ajoute des pv si self est en soin"""
        self.gagne_mouvement()
        self.se_reveille()
        if self.est_en_soin:
            self.ajoute_pv(30)
            if not self.est_blesse:
                self.en_soin = False


if __name__ == "__main__":
    Guerrier_minotaure = Unite("UNIT_MINOTAURES_WARRIOR")
    Archer_centaure = Unite("UNIT_CENTAURES_ARCHER")
    print(Guerrier_minotaure)
    print("---------")
    print(Archer_centaure)
    print("---------")
    print("Tour 1")
    Guerrier_minotaure.commence_tour()
    Archer_centaure.commence_tour()
    Guerrier_minotaure.attaque_en_duel(Archer_centaure)
    print("---------")
    print(Guerrier_minotaure)
    print("---------")
    print(Archer_centaure)
    Guerrier_minotaure.se_soigne()
    Archer_centaure.se_soigne()
    print("---------")
    print("Tour 2")
    Guerrier_minotaure.commence_tour()
    Archer_centaure.commence_tour()
    Archer_centaure.se_fortifie()
    Guerrier_minotaure.attaque_en_duel(Archer_centaure)
    print("---------")
    print(Guerrier_minotaure)
    print("---------")
    print(Archer_centaure)
