﻿from tkinter import *
from back_city import *
from front_building import *
from front_unit import *  # pas encore utilisé
from outils import charger_carte


class Interface_Ville(Frame):
    def __init__(self, master):
        Frame.__init__(self, master)
        self.master = master
        self.ville_selectionnee = None
        self.charger_images()
        self.creer_widgets()
        self.placer_widgets()


        # Foncions Annexes #

    def charger_images(self):
        self.images = dict()
        path = "../pics/window/"
        self.images["ICON_BUILD"] = PhotoImage(file=path + "ICON_BUILD_SMALL.png")
        self.images["ICON_TRAIN"] = PhotoImage(file=path + "ICON_TRAINING_SMALL.png")
        self.images["ICON_POPULATION"] = PhotoImage(file=path + "ICON_POPULATION.png")
        path = "../pics/resources/"
        self.images["ICON_WOOD"] = PhotoImage(file=path + "RESOURCE_WOOD.png")
        self.images["ICON_STONE"] = PhotoImage(file=path + "RESOURCE_STONE.png")
        self.images["ICON_GOLD"] = PhotoImage(file=path + "RESOURCE_GOLD.png")

    def creer_widgets(self):
        self.affiche_name = Label(master=self,
                                  text="Aucune ville sélectionnée",
                                  font="Arial 16 bold",
                                  justify=CENTER)
        self.affiche_population = Label(master=self,
                                        image=self.images["ICON_POPULATION"],
                                        text="?\t",
                                        compound=LEFT,
                                        justify=LEFT)
        self.affiche_prod_or = Label(master=self,
                                     image=self.images["ICON_GOLD"],
                                     text="?\t",
                                     compound=LEFT,
                                     justify=LEFT)
        self.affiche_prod_bois = Label(master=self,
                                       image=self.images["ICON_WOOD"],
                                       text="?\t",
                                       compound=LEFT,
                                       justify=LEFT)
        self.affiche_prod_pierre = Label(master=self,
                                         image=self.images["ICON_STONE"],
                                         text='?\t',
                                         compound=LEFT,
                                         justify=LEFT)
        self.affiche_builds = Label(master=self,
                                    text="Bâtiments: ?",
                                    justify=LEFT)
        self.btn_construire = Button(self,  # text="Constr.",
                                     image=self.images["ICON_BUILD"],
                                     command=lambda: self.on_btn_Constr_click())
        self.btn_entrainer = Button(self,  # text="Train",
                                    image=self.images["ICON_TRAIN"],
                                    command=lambda: self.on_btn_Train_click())

    def placer_widgets(self):
        self.affiche_name.grid(row=2, column=0, columnspan=4, sticky=EW)
        self.affiche_population.grid(row=3, column=0)
        self.affiche_prod_or.grid(row=3, column=1)
        self.affiche_prod_bois.grid(row=3, column=2)
        self.affiche_prod_pierre.grid(row=3, column=3)
        self.affiche_builds.grid(row=5, column=0, sticky=W)
        self.btn_construire.grid(row=1, column=2)
        self.btn_entrainer.grid(row=1, column=3)

        # Mutateur #
    def actualiser(self, city, hidden: bool):
        self.ville_selectionnee = city
        self.affiche_name.config(text=city.name)
        self.affiche_population.config(text=str(city.population)+"\t")
        if not hidden:
            txt = "Bâtiments: "
            for building in city.buildings:
                txt += "\n\t•" + building.name
            self.affiche_builds.config(text=txt)
            self.affiche_prod_bois.config(text="\t" + str(city.prod_bois))
            self.affiche_prod_or.config(text="\t" + str(city.prod_or))
            self.affiche_prod_pierre.config(text="\t" + str(city.prod_pierre))
        else:
            self.affiche_builds.config(text="Bâtiments: ?")
            self.affiche_prod_bois.config(text="?\t")
            self.affiche_prod_or.config(text="?\t")
            self.affiche_prod_pierre.config(text="?\t")

    # Callbacks #
    def on_btn_Constr_click(self):
        if self.ville_selectionnee is not None:
            Interface_Construction(master=self.master,
                                   city=self.ville_selectionnee)

    def on_btn_Train_click(self):
        if self.ville_selectionnee is not None:
            print("unit train menu ouverte")

    # Main #


if __name__ == "__main__":
    map_datas = charger_carte("MAP_01.json")
    ville_1 = Ville("CITY_1", map_datas)
    ville_2 = Ville("CITY_2", map_datas)

    fenetre = Tk()
    ecran = Interface_Ville(fenetre)
    btn1 = Button(fenetre,
                  text="Ville 1",
                  command=lambda: ecran.actualiser(ville_1, hidden=False))
    btn2 = Button(fenetre,
                  text="Ville 2",
                  command=lambda: ecran.actualiser(ville_2, hidden=True))

    ecran.grid(row=0, column=0, columnspan=9)
    btn1.grid(row=1, column=0)
    btn2.grid(row=1, column=1)

    # fenetre.geometry("1000x900")
    fenetre.mainloop()
