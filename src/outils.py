import json


def charger_fichier_json(filetype: str) -> dict:
    """Crée une adresse à partir du nom de fichier "DATA_" donné et récupère le fichier correspondant
    sous forme de dictionnaire.
    PARAMETRE :
        - filetype : peut valoir "BUILDINGS", "NATIONS", "RESOURCES" ou "UNITS"
    """
    fichier = open(file="../datas/DATA_" + filetype + ".json", mode="r",encoding="utf8")
    dico = json.load(fichier)
    fichier.close()
    return dico


def charger_carte(mapfile: str) -> dict:
    """Crée une adresse à partir du nom de fichier "MAPS" donné et récupère le fichier correspondant
    sous forme de dictionnaire.
    WARNING : mapfile : correspond au nom de fichier complet (extension incluse) !!!
    """
    fichier = open(file=mapfile, mode="r")
    dico = json.load(fichier)
    fichier.close()
    return dico


if __name__ == '__main__':
    # Utilisation de charger_fichier_json()
    print(charger_fichier_json("BUILDINGS"))
    print(charger_fichier_json("NATIONS"))
    print(charger_fichier_json("RESOURCES"))
    print(charger_fichier_json("UNITS"))
    print("\t"*3 + "#"*20)

    # Utilisation de charger_carte()
    print(charger_carte("MAP_01.json"))
