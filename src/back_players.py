from outils import *
from back_unit import Unite
from back_city import Ville


###CONSTRUCTEUR
class Joueur:
    def __init__(self, nation_id: str, dico: dict):
        """Initialise le joueur"""

        self.name = dico[nation_id]["NOM"]
        self.descr = dico[nation_id]["DESCRIPTION"]
        self.color = dico[nation_id]["COULEUR"]
        self.villes = []
        self.unites = []
        self.resources = {"OR": 20, "PIERRE": 20, "BOIS": 20, "SCIENCE":0}
        # Images
        self.flag = "../pics/nations/" + dico[nation_id]["DRAPEAU"]
        self.leader = "../pics/nations/" + dico[nation_id]["LEADER"]

    ###FONCTION D'AFFICHAGE
    def __str__(self)-> str:
        """Renvoie la chaîne de caractère et affiche un affichage complet du joueur"""
        txt = self.name + ":\n"
        txt += "LEADER :" + str(self.leader) + "\n"
        txt += "RESSOURCES :" + str(self.resources)[1:-1] + "\n"
        txt += "UNITES : " + str(self.unites)[1:-1] + "\n"
        txt += "VILLES : " + str(self.villes)[1:-1] + "\n"
        return txt

    def __repr__(self)-> str:
        """Renvoie chaîne de caratère et affiche un affichage succinct des joueurs."""
        return self.name

    ###ACCESSEURS
    @property
    def nb_unites(self):
        return len(self.unites)

    @property
    def nb_villes(self):
        return len(self.villes)

    def est_mort(self)-> bool:
        """Indique si le jour a perdu ou non """
        if self.nb_villes == 0 and self.nb_unites == 0:
            return True
        else:
            return False

    def a_fini_tour(self)-> bool:
        """Indique si le joueur a fini son tour de jeu ou pas"""
        if self.a_unite_en_attente() or self.a_ville_en_attente():
            return False
        else:
            return True

    ###GESTION DES VILLES :
    def villes_conquises(self)-> list:
        """Renvoie la liste des villes détenues par le joueur"""
        return self.villes

    def a_ville_en_attente(self)-> bool:
        """Indique si l'une des villes du joueur est en attente de construction"""
        for ville in self.villes:
            if ville.en_attente():
                return True
        return False

    ###GESTION DES UNITES :
    def possede_unite(self, tag: str)-> bool:
        """Indique si l'unité portant l'étiquette tag est détenue par le joueur"""
        for unite in self.unites:
            if tag == unite.etiquette:
                return True
        return False

    def cherche_unite(self, tag: str)-> Unite:
        """Renvoie l'unité portant l'étiquette tag (et None si elle n'existe pas)"""
        for unite in self.unites:
            if tag == unite.etiquette:
                return unite
        return None

    def a_unite_en_attente(self)-> bool:
        """Indique si le joueur a fini de déplacer toutes ses unités ou pas"""
        for unite in self.unites:
            if unite.en_attente():
                return True
        return False

    ### MUTATEURS
    def commence_tour(self):
        """Applique les différentes actions de début de tour aux villes et unités du joueur """
        self.recolte()
        for ville in self.villes:
            ville.commence_tour()
        for unite in self.unites:
            unite.commence_tour()

    ####GESTION DES UNITES       
    def ajoute_unite(self, unit: Unite):
        """ Ajoute l'unité indiquée de la liste des unités du joueur"""
        self.unites.append(unit)

    def enleve_unite(self, unit: Unite):
        """ Enlève l'unité indiquée de la liste des unités du joueur"""
        self.unites.remove(unit)

    ####GESTION DES RESSOURCES
    def ajoute_ressources(self, resources: dict):
        """Ajoute_ressources au joueur les ressources contenues dans le dictionnaire """
        self.resources["OR"] += resources["OR"]
        self.resources["BOIS"] += resources["BOIS"]
        self.resources["PIERRE"] += resources["PIERRE"]
        self.resources["SCIENCE"] += resources["SCIENCE"]

    def enleve_ressources(self, resources: dict):
        """Enlève au joueur les ressources contenues dans le dictionnaire"""
        if "OR" in resources:
            self.resources["OR"] = max(0, self.resources["OR"] - resources["OR"])
        if "BOIS" in resources:
            self.resources["BOIS"] = max(0, self.resources["BOIS"] - resources["BOIS"])
        if "PIERRE" in resources:
            self.resources["PIERRE"] = max(0, self.resources["PIERRE"] - resources["PIERRE"])
        if "SCIENCE" in resources:
            self.resources["SCIENCE"] = max(0, self.resources["SCIENCE"] - resources["SCIENCE"])

    def recolte(self):
        """Récolte les ressources produites par les villes et bâtiments afin de les stocker dans la réserve du joueur"""
        for ville in self.villes:
            resources = ville.recolte()
            self.ajoute_ressources(resources)

    ###GESTION DES VILLES
    def ajoute_ville(self, city: Ville):
        """Ajoute la ville indiquée de la liste des villes détenues par le joueur"""
        self.villes.append(city)

    def enleve_ville(self, city: Ville):
        """Enlève la ville indiquée de la liste des villes détenues par le joueur"""
        self.villes.remove(city)


if __name__ == "__main__":
    dico = charger_fichier_json("NATIONS")
    j1 = Joueur("NATION_MINOTAURES", dico)
    print(j1)
    j1.ajoute_unite(Unite("UNIT_MINOTAURES_WARRIOR"))
    j1.ajoute_unite(Unite("UNIT_MINOTAURES_WARRIOR"))
    j1.ajoute_unite(Unite("UNIT_MINOTAURES_ARCHER"))
    # v = Ville(, charger_carte("MAP_01.json"))
    # j1.ajoute_ville

    print("Ajout d'unités\n", j1)
