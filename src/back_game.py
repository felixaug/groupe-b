from back_city import *
from back_players import *
from outils import *


class Jeu():
    def __init__(self, map_name: str, first_player_id:str):
        map_datas = charger_carte(map_name)
        self.background = "../pics/maps/" + map_datas["MAP_INITIALIZE"]["FILE"]
        self.width = map_datas["MAP_SIZE"]["x"]
        self.height = map_datas["MAP_SIZE"]["y"]
        self.liste_villes = self.creer_villes(map_datas)
        self.liste_connexions = map_datas["CONNECTIONS"]
        self.connecter_villes(map_datas)
        self.liste_joueurs = self.creer_joueurs(first_player_id)
        self.premier_joueur = self.liste_joueurs[0]
        self.num_joueur_actif = 0
        self.num_tour = 1
        self.unite_selectionnee = None
        self.ville_selectionnee = None
        self.num_joueur_actif = 0

    def creer_villes(self, map_datas: dict):
         return {name:Ville(name, map_datas) for name in map_datas["CITY_DETAILS"]}

    def connecter_villes(self, map_datas: dict):
        #self.liste_connexions = [map_datas["CONNECTIONS"][i] for i in range(0, len(map_datas["CONNECTIONS"]))]
        for co in self.liste_connexions:
            self.liste_villes[co[0]].se_connecte_a(self.liste_villes[co[1]])

    def creer_joueurs(self, first_player_id:str):
        liste_joueurs = []
        dico = charger_fichier_json("NATIONS")
        liste_joueurs.append(Joueur(first_player_id, dico))
        for nation in dico:
            if nation != first_player_id:
                liste_joueurs.append(Joueur(nation, dico))
        return liste_joueurs


    def __str__(self):
        return f"Tour : {self.num_tour}\nJoueur:{self.joueur_actuel()}"

    def tour_actuel(self):
        return self.num_tour

    def joueur_actuel(self) -> Joueur:
        return self.liste_joueurs[self.num_joueur_actif]

    def a_fini_de_jouer(self) -> bool:
        return self.joueur_actuel().a_fini_tour()

    def est_mort(self):
        return  self.joueur_actuel().est_mort()

    def chercher_unite(self, tag:str)->Unite:
        pass

    def chercher_ville(self, city_id:str)->Ville:
        pass

    def peut_deplacer_unite(self, unit:Unite, destination:Ville)->bool:
        pass

    def joueur_suivant(self):
        self.num_joueur_actif = (self.num_joueur_actif + 1) % len(self.liste_joueurs)
        if self.num_joueur_actif == 0:
            self.num_tour += 1

    def creer_unite(self, unit_id:str):
        if self.ville_selectionnee is not None:
            u = Unite(unit_id)
            self.joueur_actuel().ajoute_unite(u)
            self.ville_selectionnee.ajoute_unite(u)

    def selectionner_unite(self, unit:Unite):
        self.unite_selectionnee = unit

    def deplacer_unite(self, city_id:str):
        pass

    def attaque_en_duel(self, city_id:str)->str:
        city = self.liste_villes[city_id]
        if self.unite_selectionnee is not None:
            if city.contient_unite():
                self.unite_selectionnee.attaque_en_duel(self, city.unite_stationee())
            else:
                self.deplacer_unite(city_id)

    def attaque_a_distance(self, city_id:str):
        pass

    def selectionner_ville(self, city_id:str):
        self.ville_selectionnee = self.liste_villes[city_id]

if __name__ == "__main__":
    monJeu = Jeu("../maps/MAP_01.json", "NATION_MINOTAURES")
    monJeu.selectionner_ville("CITY_1")
    monJeu.creer_unite("UNIT_MINOTAURES_WARRIOR")
    monJeu.creer_unite("UNIT_MINOTAURES_WARRIOR")
    print(monJeu)
