from outils import *


class Building:
    def __init__(self, building_id: str, dico:dict):
        self.name = dico[building_id]["NOM"]
        self.id = building_id
        # Coût de construction
        self.cout_bois = dico[building_id]["COUT_BOIS"]
        self.cout_pierre = dico[building_id]["COUT_PIERRE"]
        self.cout_or = dico[building_id]["COUT_OR"]
        # Production de ressources
        self.prod_bois = dico[building_id]["PROD_BOIS"]
        self.prod_pierre = dico[building_id]["PROD_PIERRE"]
        self.prod_or = dico[building_id]["PROD_OR"]
        self.prod_science = dico[building_id]["PROD_SCIENCE"]
        # Caractéristiques
        self.vie_max = dico[building_id]["VIE"]
        self.vie = dico[building_id]["VIE"]
        self.capacities = dico[building_id]["CAPACITIES"]
        self.peut_entrainer = dico[building_id]["CAN_ENTRAINED"]
        self.temps_construction_max = dico[building_id]["TIME"]
        self.temps_construction = self.temps_construction_max
        # Images
        self.icon = "../pics/buildings/" + dico[building_id]["ICON"]

    ### FONCTION D'AFFICHAGE
    def __str__(self) -> str:
        """Definit la classe avec str et renvoie l'interface complète
        et les statistiques d'un batiment"""
        txt = "\t" + self.name

        if self.est_detruit:
            txt += "\t(Détruit) :\n"
        elif self.est_endommage:
            txt += "\t(Endommagé) :\n"
        elif self.est_acheve:
            txt += " :\n"
        else:
            txt += "\t(Inachevé) :\n"


        """txt += "Production de bois :" + str(self.prod_bois) + "\n"
        txt += "Production de pierre :" + str(self.prod_pierre) + " \n"
        txt += "production d'or :" + str(self.prod_or) + " \n"
        txt += "production de science :" + str(self.prod_science) + " \n"""

        txt += "Cout de bois :" + str(self.cout_bois) + "\n"
        txt += "cout de pierre :" + str(self.cout_pierre) + " \n"
        txt += "cout d'or :" + str(self.cout_or) + " \n"

        """txt += "Vie :" + str(self.vie) + "/" + str(self.vie_max) + "\n"
        txt += "Capacités:" + str(self.capacities) + "\n"""
        return txt

    def __repr__(self) -> str:
        """Definit la classe et renvoie son nom, affichage très simple """
        return self.name

    ###ACCESSEURS
    @property
    def est_detruit(self) -> bool:
        """Indique si le batiment est detruit ou pas."""
        if self.vie == 0:
            return True
        else:
            return False

    @property
    def est_endommage(self) -> bool:
        """Indique si le batiment est endommagé ou non."""
        if 0 < self.vie < self.vie_max:
            return True
        else:
            return False

    @property
    def est_acheve(self) -> bool:
        """Indique si le batiment est achevé ou non."""
        if self.temps_construction == 0:
            return True
        else:
            return False

    def recolte(self) -> dict:
        """Renvoie en dictionnaire le contenant de la
        production d'un batiment"""
        if self.est_endommage or self.est_detruit:
            return {"OR": 0,
                    "BOIS": 0,
                    "PIERRE": 0,
                    "SCIENCE": 0
                    }
        else:
            return {"OR": self.prod_or,
                    "BOIS": self.prod_bois,
                    "PIERRE": self.prod_pierre,
                    "SCIENCE": self.prod_science
                    }

    ### ACCESSEURS OPTIONNELS
    def unites_entrainables(self) -> list:
        """Renvoie les unités que peut produire un batiment"""
        if self.peut_entrainer:
            return None

    def peut_entrainer(self, unit_id: str = "ANY") -> bool:
        """Indique si le batiment peut construire une unité."""
        return True

    ### MUTATEURS

    def ajoute_vie(self, value: int = 1):
        """Ajoute de la vie au batiment"""
        if self.est_acheve:
            self.vie += value
            if self.vie > self.vie_max:
                self.vie = self.vie_max

    def enleve_vie(self, value: int = 1):
        """Enleve de la vie au batiment."""
        if self.est_acheve:
            self.vie -= value
            if self.vie < 0:
                self.vie = 0

    def devient_endommage(self):
        """DEPRECIE"""
        pass
    def devient_acheve(self):
        """DEPRECIE"""
        pass

    def construit(self, value: int = 1):
        if not self.est_acheve:
            self.temps_construction = max(0, self.temps_construction - value)
        elif self.est_endommage:
            self.ajoute_vie(value)

    def ajoute_ressource(self, value: int, res_id: str):
        """Ajoute de des ressources au batiment"""
        assert res_id in ["OR", "PIERRE", "BOIS", "SCIENCE"]
        if res_id == "OR":
            self.prod_or += value
        elif res_id == "PIERRE":
            self.prod_pierre += value
        elif res_id == "BOIS":
            self.prod_bois += value
        else:
            self.prod_science += value

    def enleve_ressource(self, value: int, res_id: str):
        """Enleve des ressources au batiment"""
        assert res_id in ["OR", "PIERRE", "BOIS", "SCIENCE"]
        if res_id == "OR":
            self.prod_or = max(0, self.prod_or - value)
        elif res_id == "PIERRE":
            self.prod_pierre = max(0, self.prod_pierre - value)
        elif res_id == "BOIS":
            self.prod_bois = max(0, self.prod_bois - value)
        else:
            self.prod_science = max(0, self.prod_science - value)


if __name__ == "__main__":
    dico = charger_fichier_json("BUILDINGS")
    b1 = Building("BUILD_MINE", dico)
    b2 = Building("BUILD_SCIERIE", dico)
    b3 = Building("BUILD_CAMP", dico)
    b4 = Building("BUILD_BOAT", dico)
    b5 = Building("BUILD_ARMORY", dico)
    print(b1)
    print([b1, b2, b3, b4, b5])
    print(b3.name, "peut entrainer des unites :", b3.peut_entrainer)

    print("\nConstruction de ", b1.name)
    b1.construit(50)
    print(b1)

    print("\nDégats sur ", b1.name)
    b1.enleve_vie(200)
    print(b1)


