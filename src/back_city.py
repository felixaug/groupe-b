﻿from back_building import *
from back_unit import *
from outils import charger_carte, charger_fichier_json


class Ville:
    ### Contructeur ###
    def __init__(self, city_id: str, map_datas: dict):
        city_datas = map_datas["CITY_DETAILS"][city_id]
        self.id = city_id
        self.etiquette = None
        self.x = city_datas["location"]["x"]
        self.y = city_datas["location"]["y"]
        self.name = city_datas["name"]
        self.owner = city_datas["default_nation"]
        self.neighbors = []
        self.buildings = []
        self.construction_en_cours = None
        self.initialiser_batiments()
        self.units = None
        self.prod_pierre = 1
        self.prod_bois = 1
        self.prod_or = 1
        self.prod_science = 1
        self.population = 1

    def initialiser_batiments(self):
        dico = charger_fichier_json("BUILDINGS")
        for building_id in dico:
            self.buildings.append(Building(building_id, dico))

    ### Fonctions d'affichage ###
    def __str__(self):
        txt = "\t {} :\n".format(self.name)
        txt += "Unité : "
        if self.units is None:
            txt += "-\n"
        else:
            txt += self.units.name + "\n"
        txt += "Bâtiments : "
        if self.buildings == []:
            txt += "-\n"
        else:
            txt += str(self.batiments_acheves())[1:-1] + "\n"
        txt += "Voisins : " + str(self.voisins())[1:-1] + "\n"
        return txt

    def __repr__(self):
        txt = self.name
        return txt

    ##################
    ### Accesseurs ###
    ##################

    def proprietaire(self) -> str:
        if self.owner == "":
            return None
        else:
            return self.owner

    def voisins(self) -> list:
        return self.neighbors

    ### Gestion des ressources ###
    def recolte(self) -> dict:
        ressources_recoltees = {"OR": self.prod_or,
                                "BOIS": self.prod_bois,
                                "PIERRE": self.prod_pierre,
                                "SCIENCE": self.prod_science}
        for batiment in self.buildings:
            ressources_batiment = batiment.recolte()
            ressources_recoltees["OR"] += ressources_batiment["OR"]
            ressources_recoltees["BOIS"] += ressources_batiment["BOIS"]
            ressources_recoltees["PIERRE"] += ressources_batiment["PIERRE"]
            ressources_recoltees["SCIENCE"] += ressources_batiment["SCIENCE"]
        return ressources_recoltees

    ### Gestion des bâtiments ###
    def batiments_acheves(self) -> list:
        buildinglist = []
        for batiment in self.buildings:
            if batiment.est_acheve:
                buildinglist.append(batiment)
        return buildinglist

    def batiments_endommages(self) -> list:
        building_list = []
        for batiment in self.buildings:
            if batiment.est_endommage:
                building_list.append(batiment)
        return building_list

    def batiments_non_constuits(self) -> list:
        buildinglist = []
        for batiment in self.buildings:
            if not batiment.est_acheve:
                buildinglist.append(batiment)
        return buildinglist

    def en_attente(self) -> bool:
        if self.construction_en_cours is None:
            return True
        else:
            return False

    ### Gestion des unités ###
    def contient_unite(self) -> bool:
        if self.units is None:
            return True
        else:
            return False

    def unite_stationee(self) -> Unite:
        return self.units

    def peut_ajouter_unite(self) -> bool:
        if not self.contient_unite():
            return True
        else:
            return False

    def unites_entrainables(self) -> list:
        """OPTIONNEL"""
        pass

    #################
    ### Mutateurs ###
    #################

    def change_proprietaire(self, owner_id: str):
        self.owner = owner_id

    def se_connecte_a(self, other):
        self.neighbors.append(other)
        other.neighbors.append(self)

    def obtient_identifiant(self, tag: int):
        self.etiquette = tag

    def commence_tour(self):
        self.ajoute_population(1)  # TODO : penser à gérer la population
        self.construit()

    ### Gestion de population ###
    def enleve_population(self, value: int = 1):
        if self.population > value:
            self.population -= value
        else:
            self.population = 1

    def ajoute_population(self, value: int = 1):
        self.population += value

    ### Gestion de resources ###
    def ajoute_ressource(self, value: int, res_id: str):
        """from back_building: Cntl+C, Cntl+V"""
        assert res_id in ["OR", "PIERRE", "BOIS", "SCIENCE"]
        if res_id == "OR":
            self.prod_or += value
        elif res_id == "PIERRE":
            self.prod_pierre += value
        elif res_id == "BOIS":
            self.prod_bois += value
        elif res_id == "SCIENCE":
            self.prod_science += value

    def enleve_ressource(self, value: int, res_id: int):
        assert res_id in ["OR", "PIERRE", "BOIS", "SCIENCE"]
        if res_id == "OR":
            self.prod_or -= value
            if self.prod_or <= 0:
                self.prod_or = 1
        elif res_id == "PIERRE":
            self.prod_pierre -= value
            if self.prod_pierre <= 0:
                self.prod_pierre = 1
        elif res_id == "BOIS":
            self.prod_bois -= value
            if self.prod_bois <= 0:
                self.prod_bois = 1
        elif res_id == "SCIENCE":
            self.prod_science -= value
            if self.prod_science <= 0:
                self.prod_science = 1

    ### Gestion des unités ###
    def ajoute_unite(self, unit: Unite):
        self.units = unit

    def enleve_unite(self):  # , unit: Unite):
        self.units = None

    ### Gestion des bâtiments ###
    def met_en_chantier(self, bat_id: str):
        for batiment in self.batiments_non_constuits():
            if batiment.id == bat_id:
                self.construction_en_cours = batiment
                return None
        for batiment in self.batiments_endommages():
            if batiment.id == bat_id:
                self.construction_en_cours = batiment
                break

    def construit(self, value: int = 1):
        if not self.en_attente():
            self.construction_en_cours.construit(value)
            if self.construction_en_cours.est_acheve:
                self.construction_en_cours = None


if __name__ == "__main__":
    map_datas = charger_carte("MAP_01.json")
    ville1 = Ville("CITY_1", map_datas)
    ville2 = Ville("CITY_2", map_datas)
    ville3 = Ville("CITY_3", map_datas)
    ville1.se_connecte_a(ville2)
    ville1.se_connecte_a(ville3)
    print(ville1)
    print([ville1, ville2])

    ville1.met_en_chantier("BUILD_MINE")
    ville1.construit(8)
    ville1.met_en_chantier("BUILD_SCIERIE")
    ville1.construit(8)
    ville1.met_en_chantier("BUILD_CAMP")
    ville1.construit(1)
    print(ville1)
    print(ville1.name, "est en attente :", ville1.en_attente(), "\n--------")
    print(ville1.name, "construit :", ville1.construction_en_cours)
