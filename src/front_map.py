from outils import *
from back_city import *
from tkinter import *
from back_unit import *
from back_game import *


class Interface_Carte(Frame):

    def __init__(self, master, game: Jeu):
        Frame.__init__(self, master)
        self.master = master
        self.game = game
        self.charger_images()
        self.creer_fond()
        self.dessiner_villes()
        self.dessiner_routes()

    def creer_fond(self):
        self.my_canvas = Canvas(master=self.master,
                                width=self.game.width,
                                height=self.game.height,
                                bg="black")
        self.my_canvas.grid()
        self.my_canvas.create_image(0, 0,
                                    image=self.images["bg"],
                                    anchor=NW)

    def charger_images(self):
        self.images = {}
        self.images["bg"] = PhotoImage(file=self.game.background)
        self.images["fg"] = PhotoImage(file="../pics/window/ICON_CITY.png")
        datas = charger_fichier_json("UNITS")
        for unit_id in datas:
            path = "../pics/units/" + datas[unit_id]["FICHIER"]
            self.images[unit_id] = PhotoImage(file=path)

    def creer_scrollbar(self):
        pass

    #  Mutateurs #

    def dessiner_villes(self):
        for city in self.game.liste_villes.values():
            self.my_canvas.create_image(city.x,
                                        city.y,
                                        image=self.images["fg"],
                                        tag=city.id
                                        )
            self.my_canvas.create_text(city.x,
                                       city.y - 20,
                                       text=city.name,
                                       font="Arial 20")
            self.my_canvas.tag_bind(city.id, "<Button-1>", lambda evt, c=city: self.on_city_click(c))

    def dessiner_routes(self):
        for route in self.game.liste_connexions:
            city_a = self.game.liste_villes[route[0]]
            city_b = self.game.liste_villes[route[1]]
            self.my_canvas.create_line(city_a.x, city_a.y,
                                       city_b.x, city_b.y,
                                       dash=(4, 5))

    def dessiner_unite(self, unit: Unite):
        tag = self.my_canvas.create_image(unit.x, unit.y,
                                    image=self.images[unit.id])
        self.my_canvas.tag_bind(tag, "<Button-1>", lambda evt:self.on_unit_click(unit))
        unit.obtient_etiquette(tag)

    def effacer_unite(self, unit: Unite):
        self.my_canvas.delete(unit.etiquette)

    def deplacer_unite(self, unit: Unite, destination: tuple):
        self.my_canvas.moveto(unit.etiquette,
                              destination[0], destination[1])

    # callbacks #

    def on_city_click(self, city: Ville):
        self.game.selectionner_ville(city.id)
        self.master.event_generate("<<INFO-UPDATE>>")
        print(self.game.ville_selectionnee)

    def on_unit_click(self, unit: Unite):
        self.game.selectionner_unite(unit)
        self.master.event_generate("<<INFO-UPDATE>>")
        print(self.game.unite_selectionnee)


    # MAIN #


if __name__ == "__main__":
    def new_unit(screen):
        u = Unite("UNIT_MINOTAURES_WARRIOR")
        u.x = 100
        u.y = 100
        screen.dessiner_unite(u)

    monJeu = Jeu("../maps/MAP_01.json", "NATION_MINOTAURES")
    fenetre = Tk()

    ecran = Interface_Carte(fenetre, game=monJeu)
    btn1 = Button(fenetre,
                  text="Créer unité",
                  command=lambda : new_unit(ecran))
    ecran.grid(row=0, column=0, columnspan=2)
    btn1.grid(row=1, column=0)
    fenetre.mainloop()
