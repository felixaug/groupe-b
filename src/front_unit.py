from tkinter import *
from back_unit import *


class Interface_Unite(Frame):
    def __init__(self, master):
        Frame.__init__(self, master)
        self.master = master
        self.unite_selectionnee = None
        self.charger_images()
        self.creer_widgets()
        self.placer_widgets()
        self.configurer_widgets()

    # FONCTIONS AUXILIAIRES DE INIT
    def charger_images(self):
        """Charge les images"""
        path = "../pics/units/"
        self.images = dict()
        self.images[None] = PhotoImage(file=path+"UNIT_NO_UNIT.png")
        datas = charger_fichier_json("UNITS")
        for unit_id in datas:
            filename = path + datas[unit_id]["FICHIER"]
            self.images[unit_id] = PhotoImage(file=filename)
        path = "../pics/window/"
        self.images["ICON_MOVE"] = PhotoImage(file=path+"ICON_MOVE.png")
        self.images["ICON_ATTACK"] = PhotoImage(file=path+"ICON_ATTACK.png")
        self.images["ICON_SLEEP"] = PhotoImage(file=path+"ICON_SLEEP.png")
        self.images["ICON_FORTIFY"] = PhotoImage(file=path+"ICON_FORTIFY.png")
        self.images["ICON_HEAL"] = PhotoImage(file=path+"ICON_HEAL.png")

    def creer_widgets(self):
        """Crée les widgets"""
        self.label_nom = Label(self, text="Nom :")
        self.label_attaque = Label(self, text="Attaque :")
        self.label_vie = Label(self, text="Vie :")
        self.label_mouvements = Label(self, text="Mouvements :")
        self.label_icon = Label(self, image=self.images[None])

        self.barre_btn = Frame(self)
        self.btn_bouger = Button(self.barre_btn,
                                 image=self.images["ICON_MOVE"])# text="Se déplacer")
        self.btn_dormir = Button(self.barre_btn,
                                 image=self.images["ICON_SLEEP"])# text="S'endormir")
        self.btn_fortifier = Button(self.barre_btn,
                                 image=self.images["ICON_FORTIFY"])# text="Se fortifier")
        self.btn_soigner = Button(self.barre_btn,
                                 image=self.images["ICON_HEAL"])# text="Se soigner")
        self.btn_attaquer = Button(self.barre_btn,
                                 image=self.images["ICON_ATTACK"])# text="Attaquer")

    def placer_widgets(self):
        """Place les widgets"""
        self.label_nom.grid(row=0, column=0, sticky=EW)
        self.label_attaque.grid(row=1, column=0, sticky=EW)
        self.label_vie.grid(row=2, column=0, sticky=EW)
        self.label_mouvements.grid(row=3, column=0, sticky=EW)

        self.label_icon.grid(row=0, column=1,
                             rowspan=4, columnspan=5,
                             sticky=NSEW)
        self.barre_btn.grid(row=4, column=0, columnspan=2)
        self.btn_bouger.grid(row=0, column=0)
        self.btn_dormir.grid(row=0, column=1)
        self.btn_fortifier.grid(row=0, column=2)
        self.btn_soigner.grid(row=0, column=3)
        self.btn_attaquer.grid(row=0, column=4)

    def configurer_widgets(self):
        """Configure les widgets"""
        self.label_mouvements.config(justify=LEFT)
        self.label_vie.config(justify=LEFT)
        self.label_attaque.config(justify=LEFT)
        self.label_nom.config(justify=LEFT)

        self.btn_bouger.config(command=lambda: self.on_btn_bouger_click())
        self.btn_dormir.config(command=lambda: self.on_btn_dormir_click())
        self.btn_fortifier.config(command=lambda: self.on_btn_fortifier_click())
        self.btn_soigner.config(command=lambda: self.on_btn_soigner_click())
        self.btn_attaquer.config(command=lambda:self.on_btn_attaquer_click())

    # MUTATEUR
    def actualiser(self, unit: Unite):
        """Met à jour les labels de l'interface en fonction de l'unité donnée
           PARAMETRE:
           un identifiant d'unité de type 'UNIT_NATION_UNITE'
           Exemple: actualiser('UNIT_MINOTAURES_WARRIOR')
           """
        if unit is None:
            self.unite_selectionnee = None
            self.label_nom.config(text="Nom :")
            self.label_attaque.config(text="Attaque :")
            self.label_vie.config(text="Vie :")
            self.label_mouvements.config(text="Mouvements :")
            self.label_icon.config(image=self.images[None])
        else:
            self.unite_selectionnee = unit
            self.label_nom.config(text="Nom : " + self.unite_selectionnee.name)
            self.label_attaque.config(text="Attaque : " + str(self.unite_selectionnee.attack))
            self.label_vie.config(text="Vie : " + str(self.unite_selectionnee.current_hp) + "/" + str(self.unite_selectionnee.max_hp))
            self.label_mouvements.config(text="Mouvements : " + str(self.unite_selectionnee.mouvement))
            self.label_icon.config(image=self.images[unit.id])

    # CALLBACKS
    def on_btn_bouger_click(self):
        if self.unite_selectionnee is not None:
            # TODO : traiter la MAJ de la carte
            print(self.unite_selectionnee.name + " se déplace")

    def on_btn_dormir_click(self):
        if self.unite_selectionnee is not None:
            self.unite_selectionnee.s_endort()
            print(self.unite_selectionnee.name + " s'endort")

    def on_btn_fortifier_click(self):
        if self.unite_selectionnee is not None:
            self.unite_selectionnee.se_fortifie()
            print(self.unite_selectionnee.name + " se fortifie")

    def on_btn_soigner_click(self):
        if self.unite_selectionnee is not None:
            self.unite_selectionnee.se_soigne()
            print(self.unite_selectionnee.name + " se soigne")

    def on_btn_attaquer_click(self):
        if self.unite_selectionnee is not None:
            # TODO : traiter la MAJ de la carte
            print(self.unite_selectionnee.name + " attaque")


if __name__ == "__main__":
    unite_1 = Unite("UNIT_MINOTAURES_WARRIOR")
    unite_2 = Unite("UNIT_CENTAURES_ARCHER")
    unite_2.enleve_pv(20)

    fenetre = Tk()
    ecran = Interface_Unite(fenetre)
    btn1 = Button(fenetre,
                  text="Unité 1",
                  command=lambda: ecran.actualiser(unite_1))
    btn2 = Button(fenetre,
                  text="Unité 2",
                  command=lambda: ecran.actualiser(unite_2))
    ecran.grid(row=0, column=0, columnspan=2)
    btn1.grid(row=1, column=0)
    btn2.grid(row=1, column=1)
    fenetre.mainloop()
