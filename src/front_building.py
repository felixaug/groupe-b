from tkinter import *
from back_city import Ville
from back_building import *


###CLASS###
class Interface_Construction(Toplevel):
    def __init__(self, master, city: Ville):
        Toplevel.__init__(self, master)
        self.master = master
        self.ville_selectionnee = city
        self.title(self.ville_selectionnee.name)
        self.charger_images()
        self.creer_widgets()
        self.configurer_widgets()
        self.placer_widgets()
        self.mainloop()

    def charger_images(self):
        self.images = dict()
        for bati1 in self.ville_selectionnee.batiments_non_constuits():
            self.images[bati1.id] = PhotoImage(file=bati1.icon)
        self.images["ICON_OK"] = PhotoImage(file="../pics/window/ICON_BUILD_LARGE.png")

    def creer_widgets(self):
        # ZONE 1 : BÂTIMENTS A CONSTRUIRE
        self.zone1 = Frame(self)
        self.lbl_zone1 = Label(master=self.zone1,
                               text="Bâtiments à construire :")
        self.btn_batimentAC = []
        for bati in self.ville_selectionnee.batiments_non_constuits():
            btn = Button(master=self.zone1,
                         text=bati.name,
                         image=self.images[bati.id],
                         compound=BOTTOM,
                         command=lambda x=bati: self.on_batiment_click(x))
            self.btn_batimentAC.append(btn)

        # ZONE 2 : BÂTIMENTS DÉJÀ CONSTRUITS
        self.zone2 = Frame(self)
        self.lbl_zone2 = Label(master=self.zone2,
                               text="Bâtiment(s) construit(s): ")
        self.lbl_batimentC = []
        for bati in self.ville_selectionnee.batiments_acheves():
            self.lbl_batimentC.append(Label(master=self.zone2,
                                            text=bati.name))
        # ZONE DU BÂTIMENT EN COURS DE CONSTRUCTION
        if self.ville_selectionnee.construction_en_cours is None:
            txt = "Aucun bâtiment\nen cours de construction"
        else:
            txt = str(self.ville_selectionnee.construction_en_cours)
        self.lbl_batiment_encours = Label(master=self,
                                          text=txt)
        self.btn_OK = Button(master=self,
                             image=self.images["ICON_OK"])

    def configurer_widgets(self):
        self.lbl_zone1.config(font="Arial 14",
                              fg="#ff0000")
        self.lbl_zone2.config(font="Arial 14",
                              fg="#ff0000")
        self.btn_OK.config(command=self.on_btn_OK_click)

    def placer_widgets(self):
        self.zone1.grid(row=1, column=1)
        self.zone2.grid(row=2, column=1)
        self.lbl_batiment_encours.grid(row=1, column=2)
        self.btn_OK.grid(row=2, column=2)
        # BATIMENT A CONSTRUIRE : zone 1
        self.lbl_zone1.grid(row=0,
                            column=0,
                            columnspan=2)
        i = 1
        for btn in self.btn_batimentAC:
            btn.grid(row=(i+1)//2,
                     column=(i-1)%2,
                     sticky=NSEW)
            i += 1
        # BATIMENT CONSTRUIT : zone 2
        self.lbl_zone2.grid(row=0)
        i = 1
        for lbl in self.lbl_batimentC:
            lbl.grid(row=i)
            i += 1

    def on_batiment_click(self, building: Building):
        self.lbl_batiment_encours.config(text=str(building))
        self.ville_selectionnee.met_en_chantier(building.id)

    def on_btn_OK_click(self):
        self.destroy()


if __name__ == "__main__":
    dico = charger_fichier_json("BUILDINGS")
    map_datas = charger_carte("MAP_01.json")
    ville_1 = Ville("CITY_2", map_datas)
    # Ajouter des bâtiments
    ville_1.met_en_chantier("BUILD_ARMORY")
    ville_1.construit(100)
    ville_1.met_en_chantier("BUILD_MINE")
    ville_1.construit(100)

    fenetre = Tk()
    btn = Button(fenetre,
                 text="Construire",
                 command=lambda: Interface_Construction(fenetre, ville_1))
    btn.grid()
    fenetre.mainloop()
