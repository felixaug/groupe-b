import tkinter.messagebox
from tkinter import *
from tkinter.messagebox import askyesno, showerror
from tkinter.filedialog import askopenfilename
from outils import *
from front_map import *
from back_game import *


class Ecran_jeu(Frame):
    def __init__(self, master, game):
        Frame.__init__(self, master, game)
        self.master = master
        self.game = game

    def cacher(self):
        pass


class Ecran_Accueil(Frame):
    def __init__(self, master):
        Frame.__init__(self, master)
        self.master = master
        self.charger_images()
        self.creer_widgets()
        self.configurer_widgets()
        self.placer_widgets()

    def charger_images(self):
        self.images = dict()
        path = "../pics/window/"
        self.images["ICON_QUIT"] = PhotoImage(file=path + "ICON_QUIT.png")
        self.images["ICON_NEW"] = PhotoImage(file=path + "ICON_NEW.png")
        self.images["ICON_LOAD"] = PhotoImage(file=path + "ICON_LOAD.png")
        self.images["ACCUEIL"] = PhotoImage(file= "../pics/window/ACCUEIL.png")

    # Fonctions Annexes #
    def creer_widgets(self):
        """Crée les widgets"""
        self.lbl_splashscreen = Label(master=self,
                                      text="MYTHOMANIA",
                                      image=self.images["ACCUEIL"],
                                      compound=BOTTOM)

        self.btn_quit_click = Button(master=self,
                                     image=self.images["ICON_QUIT"])
        self.btn_new_click = Button(master=self,
                                    image=self.images["ICON_NEW"])
        self.btn_load_click = Button(master=self,
                                     image=self.images["ICON_LOAD"])

    def configurer_widgets(self):
        """Configure les widgets"""
        self.lbl_splashscreen.config(font="Arial 40", fg="#ff0000")
        self.btn_quit_click.config(font="Arial 14",
                                   fg="#ff0000",
                                   command=self.on_btn_quit_click
                                   )

        self.btn_new_click.config(font="Arial 14",
                                  fg="#ff0000",
                                  command=self.on_btn_new_click
                                  )
        # TODO : ajouterla commande de btn_load
        self.btn_load_click.config(font="Arial 14",
                                   fg="#ff0000")

    def placer_widgets(self):
        """Place les widgets"""
        self.lbl_splashscreen.grid(row=1, column=1, columnspan=3)
        self.btn_quit_click.grid(row=1, column=1, sticky=S)
        self.btn_new_click.grid(row=1, column=2, sticky=S)
        self.btn_load_click.grid(row=1, column=3, sticky=S)

        # Destruct

    def cacher(self):
        self.grid_forget()

    # Callbacks #
    def on_btn_quit_click(self):
        reponse = askyesno(title="Informations", message="Êtes vous sûr de vouloir fermer la page ?")
        if reponse == True:
            self.master.destroy()

    def on_btn_new_click(self):
        """Choisir la carte désirée"""
        reponse = askopenfilename(title="",
                                  parent=self.master,
                                  initialdir="../maps",
                                  filetypes=[("Cartes", "*.json")])
        if reponse:
            self.cacher()
            ecrN3 = Ecran_Choix_Peuple(self.master, reponse)
            ecrN3.grid()

    def on_btn_load_click(self):
        """Lance une partie (Sauvegardée)"""
        showerror(title="Erreur",
                  message="Impossible de charger les sauvegardes pour l'instant.")




##########CLASSE##########
class Ecran_Choix_Peuple(Frame):
    ###CONSTRUCTEURS###
    def __init__(self, master, map_adress: str):
        Frame.__init__(self, master)
        self.master = master
        self.map = map_adress
        self.charger_images()
        self.creer_widgets()
        self.configurer_widgets()
        self.placer_widgets()
        self.selected_nation = None

    def charger_images(self):
        """Charge les images (logique)"""
        self.images = dict()
        path = "../pics/window/"
        self.images["ICON_OK"] = PhotoImage(file=path + "ICON_OK.png")
        self.images["ICON_QUIT"] = PhotoImage(file=path + "ICON_QUIT.png")
        path1 = "../pics/nations/"
        self.images[None] = PhotoImage(file=path1 + "LEADER_CENTAURES.png")
        datas = charger_fichier_json("NATIONS")
        for nations_id in datas:
            filename = path1 + datas[nations_id]["LEADER"]
            self.images[nations_id] = PhotoImage(file=filename)

    # Fonctions Annexes #
    def creer_widgets(self):
        """Crée les widgets"""
        self.btn_quit = Button(master=self,
                               image=self.images["ICON_QUIT"])
        self.btn_ok = Button(master=self, text="ok")
        self.btn_cancel = Button(master=self,
                                 text="Quitter")
        # Nation#
        self.btn_nation_minos = Button(master=self,
                                       image=self.images["NATION_MINOTAURES"])
        self.btn_nation_cents = Button(master=self,
                                       image=self.images["NATION_CENTAURES"])
        self.btn_nation_gorgs = Button(master=self,
                                       image=self.images["NATION_GORGONES"])
        self.btn_nation_sirs = Button(master=self,
                                      image=self.images["NATION_SIRENES"])

        self.lbl_nation_selected = Label(master=self,
                                         text ="")

    def configurer_widgets(self):
        """Configure les widgets"""
        self.btn_quit.config(font="Arial 14",
                             fg="#ff0000",
                             command=self.on_btn_quit_click
                             )
        self.btn_cancel.config(font="Arial 14",
                               fg="#ff0000",
                               command=self.on_btn_cancel_click)
        self.btn_ok.config(font="Arial 14",
                            fg="#ff0000",
                            command=self.on_btn_ok_click)
        # Nation#
        self.btn_nation_minos.config(font="Arial 14",
                                     fg="#ff0000",
                                     command=lambda: self.on_btn_nation_click("NATION_MINOTAURES"),
                                     text= "Minotaurs")
        self.btn_nation_cents.config(font="Arial 14",
                                     fg="#ff0000",
                                     command=lambda: self.on_btn_nation_click("NATION_CENTAURES"))
        self.btn_nation_gorgs.config(font="Arial 14",
                                     fg="#ff0000",
                                     command=lambda: self.on_btn_nation_click("NATION_GORGONES"))
        self.btn_nation_sirs.config(font="Arial 14",
                                     fg="#ff0000",
                                     command=lambda: self.on_btn_nation_click("NATION_SIRENES"))
        self.lbl_nation_selected.config ()

    def placer_widgets(self):
        """Place les widgets"""
        self.btn_quit.grid(row=0, column=1)
        self.btn_cancel.grid(row=1, column=1)
        self.btn_ok.grid(row=2, column=1)
        # Nation#
        self.btn_nation_minos.grid(row=0, column=2, rowspan=3)
        self.btn_nation_cents.grid(row=0, column=3, rowspan=3)
        self.btn_nation_gorgs.grid(row=0, column=4, rowspan=3)
        self.btn_nation_sirs.grid(row=0, column=5, rowspan=3)

        self.lbl_nation_selected.grid( row=3, column=1, columnspan=5)

    ####DESTRUCTEUR###
    def cacher(self):
        """Cache le contenu de la fenetre"""
        self.grid_forget()

    ###CALLBACKS###

    def on_btn_quit_click(self):
        """Page de verification afin de savoir si le
        joueur souhaite bien quitter l'interface"""
        reponse = askyesno(title="Informations", message="Êtes vous sûr de vouloir fermer la fenetre ?")
        if reponse == True:
            self.master.destroy()
    # Nation#
    def on_btn_cancel_click(self):
        """Cache l'interface et réaffiche l'écran d'accueil."""
        self.cacher()
        ecrN2 = Ecran_Accueil(self.master)
        ecrN2.grid()



    def on_btn_nation_click(self, nation_id: str):
        self.selected_nation = nation_id
        datas = charger_fichier_json("NATIONS")
        self.lbl_nation_selected.config(text=datas[self.selected_nation]["DESCRIPTION"])

    def on_btn_ok_click(self):
        if self.selected_nation is not None:
            jeu = Jeu(self.map, self.selected_nation)
            self.cacher()
            ecran = Interface_Carte(self.master, jeu)
            ecran.grid()
        else:
            self.lbl_nation_selected.config(text="Sélectionnez un peuple.")


if __name__ == "__main__":
    fenetre = Tk()
    ecrN = Ecran_Accueil(fenetre)
    ecrN.grid()
    fenetre.mainloop()
